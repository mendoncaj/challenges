# README #

This project contains 2 challenges, Text Occurrence Finder and Update Server Strategy.

#### Stack: ####
* Java 8
* JUnit 5
* Maven 3


### Update Server Strategy ###
Update Servers following a Grid Network. Should return an integer representing the minimum of days required to update all the servers. 0 represents an out of of date server and the value 1 represents an updated servers.

##### Algorithm Purposed #####

* countingInitialUpdates: Count the number of servers that has been already updated
* Until all servers has been became update or until all servers has been inspected call function dailyUpdate
* For each server updated increasing the initial counting number of updates
* If the number of servers updates is equal the total numbers of servers the application ends.

##### Time Complexity #####
 - countingInitialUpdates: O(n)
 - isCompleted: O(1)
 - dailyUpdate: O(n * m)

### Text Occurrence Finder ###

Find toys occurrences in a list of quotes.

##### Algorithm Purposed #####
* Navigate through all toys and sum up the occurrence of them into the list of quotes
* Sort the toys occurrences in descending order or in name ascending in case of equal occurrences

##### Time Complexity #####
- getOccurrencesTable: O(n * m)
- sortOccurrencesTable: O(log n)

### Tests ###

* The tests under package **src/test/java** was developed through JUnit 5.x framework