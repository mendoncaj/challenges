import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextOccurrencesTest {

    static class Input {
        String[] quotes, toys;

        Input( String []quotes, String []toys ) {
            this.quotes = quotes;
            this.toys   = toys;
        }
    }

    private static Stream<Arguments> quoteToysProvided() {
        Input sample1 = new Input(
            new String[] {
                "jenga é bom",
                "o jogo de domino é mais legal que jenga",
                "Ao contrário do que se acredita, Lorem Ipsum não é domino um texto randômico. Com mais de 2000",
                "Lorem Ipsum não é simplesmente um baralho randômico",
                "Jenga não é simplesmente um texto randômico"
            },
            new String[] {
                "jenga", "domino", "baralho"
            }
        );

        Input sample2 = new Input(
            new String[] {
                    "americanDoll é bom",
                    "o jogo de americanDoll é mais legal que americanDoll",
                    "Ao contrário do que se acredita, Lorem Ipsum não é domino um texto randômico. Com mais de 2000",
                    "Lorem Ipsum não é simplesmente um baralho randômico",
                    "americanDoll não é simplesmente um texto randômico"
            },
            new String[] {
                    "americanDoll", "domino"
            }
        );

        Input sample3 = new Input(
            new String[] {
                    ""
            },
            new String[] {
                    "jenga", "domino", "baralho"
            }
        );

        Input sample4 = new Input(
            new String[] {
                    "americanDoll é bom",
                    "o jogo de americanDoll é mais legal que americanDoll",
                    "Ao contrário do que se acredita, Lorem Ipsum não é domino um texto randômico. Com mais de 2000",
                    "Lorem Ipsum não é simplesmente um baralho randômico",
                    "americanDoll não é simplesmente um texto randômico"
            },
            new String[] {
                    ""
            }
        );

        Input sample5 = new Input(
                new String[] {
                        "americanDoll é bom",
                        "o jogo de americanDoll é legal",
                        "Ao contrário do que se acredita, Lorem Ipsum não é domino um texto americanDoll. Com mais de 2000",
                        "Lorem Ipsum não é simplesmente um americanDoll randômico",
                        "americanDoll não é simplesmente um texto randômico"
                },
                new String[] {
                        "americanDoll"
                }
        );


        return Stream.of(
                Arguments.of( sample1, "[jenga=3, domino=2, baralho=1]" ),
                Arguments.of( sample2, "[americanDoll=3, domino=1]" ),
                Arguments.of( sample3, "[baralho=0, domino=0, jenga=0]" ),
                Arguments.of( sample4, "[]" ),
                Arguments.of( sample5, "[americanDoll=5]" )
        );
    }

    @ParameterizedTest
    @MethodSource("quoteToysProvided")
    public void should_return_a_sorted_table_with_toys_occurrences_in_the_quotes(
            final Input input, final String expected ) {
        assertEquals( expected, TextOccurrences.getSortedOccurrencesTable(
                input.toys, input.quotes ).toString() );
    }
}
