import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UpdateServerTest {

    static class Input {
        int rows, cols;
        final List<List<Integer>> grid;

        Input( int rows, int cols, Integer[][] arr ) {
            this.rows = rows;
            this.cols = cols;
            this.grid = getNewGrid( rows );

            for ( int i = 0; i < rows; i++ ) {
                grid.get( i ).addAll(  Arrays.asList( arr[ i ] ) );
            }
        }
    }

    private static List<List<Integer>> getNewGrid( int rows ) {
        final List<List<Integer>> grid = new ArrayList<>();
        for ( int i = 0; i < rows; i++ ) {
            grid.add( new ArrayList<>() );
        }
        return grid;
    }

    private static Stream<Arguments> gridProvided() {
        Input sample1 = new Input( 4, 5, new Integer[][] {
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1 },
                { 0, 0, 1, 0, 0 },
        });

        Input sample2 = new Input( 4, 5, new Integer[][] {
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 1, 0, 0 },
        });

        Input sample3 = new Input( 4, 5, new Integer[][] {
                { 0, 1, 1, 0, 1 },
                { 0, 1, 0, 1, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0 },
        });

        Input sample4 = new Input( 4, 5, new Integer[][] {
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 1, 0, 0 },
        });

        Input sample5 = new Input( 2, 2, new Integer[][] {
                { 0, 0 },
                { 0, 1 }
        });

        Input sample6 = new Input( 4, 5, new Integer[][] {
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1 },
        });

        Input sample7 = new Input( 3, 3, new Integer[][] {
                { 1, 0, 0 },
                { 0, 0, 0 },
                { 0, 0, 0 },
                { 0, 0, 0 },
        });

        return Stream.of(
                Arguments.of( sample1, 5 ),
                Arguments.of( sample2, 5 ),
                Arguments.of( sample3, 3 ),
                Arguments.of( sample4, 5 ),
                Arguments.of( sample5, 2 ),
                Arguments.of( sample6, 7 ),
                Arguments.of( sample7, 4 )
        );
    }

    @ParameterizedTest
    @MethodSource("gridProvided")
    void should_return_the_minimum_days_to_update_servers( final Input input, int expected ) {
        assertEquals( expected, UpdateServer.updateServers( input.rows, input.cols, input.grid ) );
    }
}