import java.util.*;

public class TextOccurrences {

    private static class CustomComparator implements Comparator<Map.Entry<String, Long>> {
        public int compare(Map.Entry<String, Long> o1, Map.Entry<String, Long> o2) {
            int r = o2.getValue().compareTo( o1.getValue() );
            if ( r == 0 ) {
                r = o1.getKey().compareTo( o2.getKey() );
            }
            return r;
        }
    }
    
    private static Map<String, Long> getOccurrencesTable( final String[] toys, final String[] quotes ) {
        final Map<String, Long> summary = new HashMap<>();
        for ( String toy: toys ) {
            if ( !toy.isEmpty() ) {
                long count = summary.getOrDefault( toy, 0L );
                summary.put( toy, count );
                for ( String quote: quotes ) {
                    if ( quote.toLowerCase().contains( toy.toLowerCase() ) ) {
                        summary.put( toy, ++count );
                    }
                }
            }
        }
        return summary;
    }

    private static Set<Map.Entry<String, Long>> sortOccurrencesTable(final Map<String, Long> summary ) {
        final Set<Map.Entry<String, Long>> sortedSummary = new TreeSet<>( new CustomComparator() );
        sortedSummary.addAll( summary.entrySet() );
        return sortedSummary;
    }

    static Set<Map.Entry<String, Long>> getSortedOccurrencesTable(
            final String[] toys, final String[] quotes ) {
        return sortOccurrencesTable( getOccurrencesTable( toys, quotes ) );
    }
}
