import java.util.List;

public class UpdateServer {

    private static int countingInitialUpdates( int rows, int cols, final List<List<Integer>> grid ) {
        int updates = 0;
        for ( int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if ( grid.get( i ).get( j ) == 1 ) {
                    ++updates;
                }
            }
        }
        return updates;
    }

    private static boolean isCompleted( int serversUpdated, int amountServers ) {
       return serversUpdated == amountServers;
    }

    private static boolean eligibleToUpdate( int rowIndex, int colIndex,
                                             final List<List<Integer>> grid, final boolean[][] visited ) {
        int rowSize = grid.size();
        int colSize = grid.get( 0 ).size();

        if ( !( rowIndex >= 0 ) || !( rowIndex <= rowSize - 1 ) ) {
            return false;
        }

        if ( !( colIndex >= 0 ) || !( colIndex <= colSize - 1 ) ) {
            return false;
        }
        return grid.get( rowIndex ).get( colIndex ) == 0 && !visited[ rowIndex ][ colIndex ];
    }

    private static void markToUpdate( int rowIndex, int colIndex,
                                      final List<List<Integer>> grid, final boolean[][] visited ) {
        grid.get( rowIndex ).set( colIndex, 1 );
        visited[ rowIndex ][ colIndex ] = true;
    }

    private static boolean isMasterServer( int rowIndex, int colIndex,
                                 final List<List<Integer>> grid, final boolean[][] visited ) {
        boolean master = grid.get( rowIndex ).get( colIndex ) == 1
                && !visited[ rowIndex ][ colIndex ];

        if ( master ) {
            visited[ rowIndex ][ colIndex ] = true;
        }
        return master;
    }

    private static int dailyUpdate( int rows, int cols, final List<List<Integer>> grid, int currentUpdated ) {
        final boolean[][] visited = new boolean[ rows ][ cols ];
        int serversUpdated = 0;

        serversUpdated += currentUpdated;
        for ( int i = 0; i < rows; i++ ) {
            for ( int j = 0; j < cols; j++ ) {
                int up      = i - 1;
                int right   = j + 1;
                int down    = i + 1;
                int left    = j - 1;

                if ( isMasterServer( i, j, grid, visited ) ) {
                    if ( eligibleToUpdate( up, j, grid, visited ) ) {
                        markToUpdate( up, j, grid, visited );
                        serversUpdated++;
                    }

                    if ( eligibleToUpdate( i, right, grid, visited ) ) {
                        markToUpdate( i, right, grid, visited );
                        serversUpdated++;
                    }

                    if ( eligibleToUpdate( down, j, grid, visited ) ) {
                        markToUpdate( down, j, grid, visited );
                        serversUpdated++;
                    }

                    if ( eligibleToUpdate( i, left, grid ,visited ) ) {
                        markToUpdate( i, left, grid, visited );
                        serversUpdated++;
                    }
                }
            }

            if ( isCompleted( serversUpdated, rows * cols ) ) {
                return serversUpdated;
            }
        }
        return serversUpdated;
    }

    static int updateServers( int rows, int cols, final List<List<Integer>> grid ) {
        int days = 0;
        final int amountServers = rows * cols;

        boolean isComplete;
        int serversUpdated = countingInitialUpdates( rows, cols, grid );
        do {
            serversUpdated = dailyUpdate( rows, cols, grid, serversUpdated );
            ++days;
            isComplete = isCompleted( serversUpdated, amountServers );
        } while( !isComplete && days < amountServers );

        return isComplete ? days : -1;
    }
}
